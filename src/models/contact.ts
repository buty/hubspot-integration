import { Model, DataTypes } from 'sequelize';
import { sequelize } from '.';
import Ticket from './ticket';

export interface ContactAttributes {
  id: number;
  firstname: string;
  lastname: string;
  createdate: string;
  lastmodifieddate: string;
}

interface ContactAttributesModel extends ContactAttributes, Model {}

const Contact = sequelize.define<ContactAttributesModel>(
  'Contact',
  {
    id: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      type: DataTypes.INTEGER,
      unique: true
    },
    firstname: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    lastname: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    createdate: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    lastmodifieddate: {
      allowNull: false,
      type: DataTypes.TEXT
    }
  },
  {
    createdAt: false
  }
);

Contact.hasMany(Ticket, {
  sourceKey: 'id',
  foreignKey: 'contactId',
  as: 'tickets'
});

Ticket.belongsTo(Contact, {
  foreignKey: 'contactId',
  as: 'contacts'
});

export default Contact;
