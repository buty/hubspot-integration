import * as hubspot from '@hubspot/api-client';
import { ContactAttributes } from './models/contact';
import { TicketAttributes } from './models/ticket';

export const hsContactToContactAttributes = (
  hsContact: hubspot.contactsModels.SimplePublicObject
): ContactAttributes => {
  return {
    id: parseInt(hsContact.id),
    firstname: hsContact.properties.firstname,
    lastname: hsContact.properties.lastname,
    createdate: hsContact.properties.createdate,
    lastmodifieddate: hsContact.properties.lastmodifieddate
  };
};

export const hsTicketToTicketAttributes = (
  hsTicket: hubspot.ticketsModels.SimplePublicObject
): TicketAttributes => {
  const results = hsTicket.associations?.contacts?.results || [];
  const contactId = results.length > 0 ? parseInt(results[0]?.id) : null;

  return {
    id: parseInt(hsTicket.id),
    content: hsTicket.properties.content,
    contactId,
    hs_lastmodifieddate: hsTicket.properties.hs_lastmodifieddate
  };
};
