import { Model, DataTypes } from 'sequelize';
import { sequelize } from '.';

export interface TicketAttributes {
  id: number;
  content: string;
  contactId: number;
  hs_lastmodifieddate: string;
}

interface TicketAttributesModel extends TicketAttributes, Model {}

const Ticket = sequelize.define<TicketAttributesModel>(
  'Ticket',
  {
    id: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      type: DataTypes.INTEGER,
      unique: true
    },
    content: { allowNull: true, type: DataTypes.TEXT },
    contactId: {
      allowNull: true,
      type: DataTypes.INTEGER
    },
    hs_lastmodifieddate: {
      allowNull: false,
      type: DataTypes.TEXT
    }
  },
  {
    createdAt: false
  }
);

export default Ticket;
