# Integration to HubSpot

## Install

Run MySql docker image

`docker run -p 5433:3306 -d --name integration-db -e MYSQL_ROOT_PASSWORD=password mysql:8.0`

Connect to database, all variables are in `.env` file.
`mysql --host 127.0.0.1 -P 5433 --user root -p`

Install dependencies
`npm install`

Run in development mode
`npm start`

Build project
`npm run build`

## Description

Script fetches data from hubspot in batches. One api call allows 100 results per page. After retrieving 5 pages, data are stored in database and this process is executed until all data are synced.

New row is added to database if `id` of Contact/Ticket is not found, otherwise `lastmodified` property is checked if data were updated and updates record accordingly.

With every sync run `updatedAt` column is updated and based on this value records are removed from database before every sync.

## Improvements

In HubSpot Ticket can be assigned to multiple Contacts. However my understanding of assigment was to add one Contact to Ticket - only first Contact is stored into Tickets table.

### Handlig errors

In assigment there was no mention how to handle errors. This results only in basic error checking.

### Tests

😓
