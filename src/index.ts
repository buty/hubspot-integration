require('dotenv').config();
import * as moment from 'moment';
import { getTicketsInBatch, getContactsInBatch } from './hubspot';
import { Op } from 'sequelize';
import { sequelize } from './models';
import Contact from './models/contact';
import Ticket from './models/ticket';
const cron = require('node-cron');

const syncContacts = async () => {
  let lastSyncedContactId = undefined;
  while (lastSyncedContactId !== null) {
    try {
      const [contacts, lastContactId] = await getContactsInBatch(
        lastSyncedContactId
      );
      lastSyncedContactId = lastContactId;

      contacts.forEach(async (contact) => {
        const [model, created] = await Contact.findOrCreate({
          where: {
            id: contact.id
          },
          defaults: contact
        });

        if (!created) {
          if (
            new Date(model.lastmodifieddate) !==
            new Date(contact.lastmodifieddate)
          ) {
            model.update(contact);
          }
        }
      });

      if (lastSyncedContactId === null) console.log('CONTACTS SYNCED');
    } catch (e) {
      console.error(e);
    } finally {
      lastSyncedContactId = null;
    }
  }
};

const syncTickets = async () => {
  let lastSyncedTicketId = undefined;
  while (lastSyncedTicketId !== null) {
    try {
      const [tickets, lastTicketId] = await getTicketsInBatch(
        lastSyncedTicketId
      );
      lastSyncedTicketId = lastTicketId;

      tickets.forEach(async (ticket) => {
        const [model, created] = await Ticket.findOrCreate({
          where: {
            id: ticket.id
          },
          defaults: ticket
        });

        if (!created) {
          if (
            new Date(model.hs_lastmodifieddate) !==
            new Date(ticket.hs_lastmodifieddate)
          ) {
            model.update(ticket);
          }
        }
      });

      if (lastSyncedTicketId === null) console.log('TICKETS SYNCED');
    } catch (e) {
      console.error(e);
    } finally {
      lastSyncedTicketId = null;
    }
  }
};

(async () => {
  try {
    await sequelize.authenticate();
    const { DB_NAME, DB_HOST, DB_PORT } = process.env;
    console.log(`Connected to ${DB_NAME}, host: ${DB_HOST}:${DB_PORT}\n`);

    await Contact.sync();
    await Ticket.sync();

    console.log('DB initial sync');
  } catch (e) {
    console.error('DB initial sync failed');
    console.error(e);
    process.exit();
  }

  await syncContacts();
  await syncTickets();

  // run every 10 minutes
  cron.schedule(`0 */${process.env.CRON_TIME} * * * *`, async () => {
    const lastUpdate = moment()
      .subtract(process.env.CRON_TIME, 'minutes')
      .toDate();

    Contact.destroy({
      where: {
        updatedAt: {
          [Op.lte]: lastUpdate
        }
      }
    });

    Ticket.destroy({
      where: {
        updatedAt: {
          [Op.lte]: lastUpdate
        }
      }
    });

    await syncContacts();
    await syncTickets();
  });
})();
