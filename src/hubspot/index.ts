require('dotenv').config();
import * as hubspot from '@hubspot/api-client';
import { ContactAttributes } from '../models/contact';
import { TicketAttributes } from '../models/ticket';
import {
  hsContactToContactAttributes,
  hsTicketToTicketAttributes
} from '../utils';

const MAX_OBJECTS_PER_PAGE = 100;
const API_CALLS_PER_BATCH = 5;

const hubspotClient = new hubspot.Client();

hubspotClient.setApiKey(process.env.HS_API_KEY);

const getContactPage = async (
  lastContactId?: string
): Promise<hubspot.contactsModels.CollectionResponseSimplePublicObject> => {
  return (
    await hubspotClient.crm.contacts.basicApi.getPage(
      MAX_OBJECTS_PER_PAGE,
      lastContactId
    )
  ).body;
};

export const getContactsInBatch = async (
  lastContactId?: string
): Promise<[ContactAttributes[], string]> => {
  const contacts = [];
  let after = lastContactId;

  for (let i = 0; i < API_CALLS_PER_BATCH; i++) {
    const { results, paging } = await getContactPage(after);
    contacts.push(
      ...results.map((contact) => hsContactToContactAttributes(contact))
    );
    after = paging?.next?.after || null;

    if (after === null) break;
  }

  return [contacts, after];
};

const getTicketPage = async (
  lastTicketId?: string
): Promise<hubspot.ticketsModels.CollectionResponseSimplePublicObject> => {
  return (
    await hubspotClient.crm.tickets.basicApi.getPage(
      MAX_OBJECTS_PER_PAGE,
      lastTicketId,
      undefined,
      ['contacts']
    )
  ).body;
};

export const getTicketsInBatch = async (
  lastTicketId?: string
): Promise<[TicketAttributes[], string]> => {
  const tickets = [];
  let after = lastTicketId;

  for (let i = 0; i < API_CALLS_PER_BATCH; i++) {
    const { results, paging } = await getTicketPage(after);
    tickets.push(
      ...results.map((ticket) => hsTicketToTicketAttributes(ticket))
    );
    after = paging?.next?.after || null;

    if (after === null) break;
  }

  return [tickets, after];
};
